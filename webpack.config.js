const HtmlPlugin = require('html-webpack-plugin')
const CssExtractPlugin = require('mini-css-extract-plugin')
const FaviconsPlugin = require('favicons-webpack-plugin')

const prod = process.env.NODE_ENV === 'production'

const url_loader = {
    loader: 'url-loader',
    options: {limit: 2048, fallback: 'file-loader?name=static/[contenthash].[ext]'},
}

module.exports = {
    mode: prod ? 'production' : 'development',

    entry: [
        './src/js/index.ts',
        './src/css/index.scss',
    ],

    output: {
        filename: '[hash:7].js',
        chunkFilename: '[chunkhash:7].js',
    },

    resolve: {
        extensions: ['.js', '.ts'],
    },

    devServer: {
        overlay: true,
    },

    module: {
        rules: [
            {test: /\.js$/, use: ['babel-loader']},
            {test: /\.tsx?$/, use: ['babel-loader', 'ts-loader']},
            {test: /\.(gif|png|jpe?g|svg|ttf)$/i, use: [url_loader]},

            {
                test: /\.(s[ac]|c)ss$/,
                use: [prod ? CssExtractPlugin.loader : 'style-loader', 'css-loader', 'sass-loader'],
            },

            {
                test: /\.html$/,
                use: [
                    'html-loader',
                    {
                        loader: 'posthtml-loader',
                        options: {
                            plugins: [
                                require('posthtml-include')({root: './src/html'}),
                                require('posthtml-extend')({root: './src/html'}),
                            ],
                        },
                    },
                ],
            },

        ],
    },

    plugins: [
        new HtmlPlugin({template: './src/html/index.html'}),
        !prod ? null : new CssExtractPlugin({filename: '[contentHash:7].css', chunkFilename: '[contentHash:7].css'}),
        new FaviconsPlugin('./src/img/favicon.jpg'),
    ].filter(plugin => plugin),
}
